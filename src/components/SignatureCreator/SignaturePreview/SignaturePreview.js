import React, { Component } from 'react';
import logo from '../../../logo.png';
import './SignaturePreview.css';

class SignaturePreview extends Component {
  render() {
    const { data } = this.props;

    return (
      <div className='SignaturePreview'>
        <img src={logo} alt='Logo' className='SignaturePreview-image'/>
        <div className='SignaturePreview-data'>
          <h3 className='SignaturePreview-title'>{data.name}</h3>
          <p className='SignaturePreview-position'>{data.position}, Hola!</p>
          <a href='http://hola.org/'>http://hola.org/</a>
        </div>
      </div>
    );
  }
}

export default SignaturePreview;
